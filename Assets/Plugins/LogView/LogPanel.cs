﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LogPanel : MonoBehaviour {
    [SerializeField]
    private Button _LogClear = null;


    // Use this for initialization
    void Start() {
        _LogClear.onClick.AddListener(() =>{ LogView.Clear(); });
    }

    // Update is called once per frame
    void Update() {
    }
}
