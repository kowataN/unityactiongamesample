﻿/* 
 * バトル状態遷移
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using State;

public class StateBattle : StateBase {
    public StateBattle(executeState execute) : base(execute) {
        Debug.Log("***** StateBattle::StateBattle");
    }

    public override void Execute() {
        if (ExecDelegate != null) {
            ExecDelegate();
        }
    }
}
