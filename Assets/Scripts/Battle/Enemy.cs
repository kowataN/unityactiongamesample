﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class Enemy : MonoBehaviour {
    private GameObject _Target;
    [SerializeField] private GameObject _ShotPrefabs = null;

    public enum IntervalType { Normal, Random };
    private float _Speed = 10.0f;
    private float _Interval = 1.0f;

    public enum State { Init, Run, Stop };
    private State _State = State.Init;
    public State state {
        get { return _State; }
        set { _State = value; }
    }

    void UpdateRotation() {
        if (_Target == null) {
            LogView.Log("Enemy::UpdateRotation Target is null");
            return;
        }
        var targetPos = _Target.transform.position - this.transform.position;
        var look = Quaternion.LookRotation(targetPos);
        this.transform.localRotation = look;
    }

    void Shot() {
        Vector3 ShotPos = new Vector3(
            this.gameObject.transform.position.x,
            this.gameObject.transform.position.y,
            this.gameObject.transform.position.z
        );
        GameObject shotPrefab = Instantiate(_ShotPrefabs, ShotPos, transform.rotation);
        if (shotPrefab != null) {
            Shot shot = shotPrefab.GetComponent<Shot>();
            shot.Init(_Speed);
        }
    }

    public void Init(GameObject target, IntervalType type, float interval, float ShotSpeed) {
        LogView.Log("**** interval : " + interval.ToString());
        _Target = target;
        _Interval = type == IntervalType.Normal
                                        ? interval
                                        : UnityEngine.Random.Range(1, _Interval);
        _Speed = ShotSpeed;
        if (_Target == null) {
            LogView.Log("Enemy::Init Target is null");
            return;
        }

        state = State.Run;

        // プレイヤーの方を向くようにする
        this.UpdateAsObservable()
            .Where(_ => _State == State.Run)
            .Subscribe(_ => UpdateRotation())
            .AddTo(gameObject);

        // 一定間隔で弾を撃つ
        Observable.Interval(TimeSpan.FromSeconds(_Interval))
                  .Where(_ => state == State.Run)
                  .Subscribe(_ => Shot())
                  .AddTo(gameObject);
    }
}
