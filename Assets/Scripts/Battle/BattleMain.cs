﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;
using System.Threading;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(InputManager))]
public class BattleMain : MonoBehaviour {
    // 簡易的にstate管理
    enum State { Init, Main, GameOver, End }
    //private State _State = State._Init;

    private MainStage _Stage = null;
    [SerializeField] private GameObject _PanelCommon = null;
    [SerializeField] private Text _TextTimer = null;
    private int _MaxTime = 10;
    private int _IntervalTimer = 1;
    [SerializeField] private Text _TextHp = null;
    private Timer _Timer = null;

    private StateBattle _StateBattle;

    private void Awake() {
        Debug.Log("GameMain::Awake");

        GameObject objStage = GameObject.Find("MainStage");
        if (objStage == null) {
            Debug.Log("OBJ MainStage is null");
            return;
        }
        _Stage = objStage.GetComponent<MainStage>();
        if (_Stage == null) {
            Debug.Log("_Stage is null");
            return;
        }
        InitBtnTitle();

        _PanelCommon.SetActive(false);
    }
    // Use this for initialization
    void Start() {
        LogView.Log("GameMain::Start");

        Fader.Instance.Init();
        Fader.Instance.SetColorAndActive(Color.black, true);
        _StateBattle = new StateBattle(StateInit);

        // 入力初期化
        InputManager.Instance.Init();
        InputManager.Instance.SetEnabledKeyborad(true);

        this.UpdateAsObservable()
            .Subscribe(_ => _StateBattle.Execute());
    }
    /// <summary>
    /// バトル初期化を行います
    /// </summary>
    private void BattleInit() {
        // ステージ初期化
        _Stage.Init();
        // タイマー初期化
        InitTimer();
        // プレイヤーHP初期化
        InitPlayerHp();
        DebugUI.Instance.Init();
    }
    /// <summary>
    /// タイマー初期化※外部に出す
    /// </summary>
    private void InitTimer() {
        LogView.Log("GameMain::InitTimer");
        if (_TextTimer == null) { return; }
        _Timer = _TextTimer.GetComponentInChildren<Timer>();
        _Timer.InitTimer(_MaxTime, _IntervalTimer);
        // 画面更新
        _Timer.Second.SubscribeToText(_TextTimer);

        // 時間切れになったら一回だけ停止処理を行う
        _Timer.Second
              .Where(x => x == 0)
              .First()
              .Subscribe(_ => {
                  _Stage.StopEnemy();
                  DispGameClear();
              })
              .AddTo(gameObject);
    }
    /// <summary>
    /// プレイヤーHP初期化
    /// </summary>
    private void InitPlayerHp() {
        LogView.Log("GameMain::InitPlayerHp");
        // UI更新
        _Stage.GetPlayer().HP
              .SubscribeToText(_TextHp);
        // HPが0になったら一回だけ停止処理を行う
        _Stage.GetPlayer().HP
              .Where(x => x == 0)
              .First()
              .Subscribe(_ => {
                  Stop();
                  DispGameOver();
              })
              .AddTo(gameObject);
    }
    public void Stop() {
        _Stage.StopEnemy();
        _Timer.Stop();
    }
    public void ReStart() {
        _Stage.StartEnemy();
        _Timer.ReStart();
    }

    private void DispGameOver() {
        if (_PanelCommon.activeSelf == false) {
            Text text = _PanelCommon.GetComponentInChildren<Text>();
            text.text = "ゲームオーバー";
        }
        _PanelCommon.SetActive(true);
    }
    private void DispGameClear() {
        if (_PanelCommon.activeSelf == false) {
            Text text = _PanelCommon.GetComponentInChildren<Text>();
            text.text = "ゲームクリア";
        }
        _PanelCommon.SetActive(true);
    }

    void InitBtnTitle() {
        Debug.Log("InitBtnTitle");
        Button btn = _PanelCommon.gameObject.GetComponentInChildren<Button>();
        if (btn == null) { Debug.Log("ffffffffff"); return; }

        btn.OnClickAsObservable()
           .Subscribe(_ => StateWhiteOut())
           .AddTo(gameObject);
    }

    #region state
    private void StateInit() {
        Debug.Log("***** State::StateInit");
        Fader.Instance.SetColorAndActive(Color.black, true);
        BattleInit();
        Stop();
        _StateBattle.ChangeState(StateFadeInInit);
    }
    private void StateFadeInInit() {
        Debug.Log("***** State::StateFadeInInit");
        Fader.Instance.BlackIn(3.0f, StateFadeInEnd);
        _StateBattle.ChangeState(null);
    }
    private void StateFadeInEnd() {
        Debug.Log("***** State::StateFadeInEnd");
        Fader.Instance.SetActiveFade(false);

        string prefabPath = "Prefabs/CountDownCanvas";
        GameObject obj = Resources.Load(prefabPath) as GameObject;
        obj = Instantiate(obj, transform.position, transform.rotation);
        Animator anim = obj.GetComponent<Animator>();
        CountDown countdown = obj.GetComponent<CountDown>();
        if (countdown != null) {
            countdown.ExecDelegate = StateCountDownEnd;
        } else {
            Debug.Log("CountDown class not found");
        }

        _StateBattle.ChangeState(null);
    }
    private void StateCountDownEnd() {
        Debug.Log("***** State::StateCountDownEnd");
        ReStart();
        _StateBattle.ChangeState(StateBattleIdle);
    }
    private void StateBattleIdle() {}
    private void StateWhiteOut() {
        Debug.Log("***** State::StateCountDownEnd");
        Fader.Instance.WhiteOut(2.0f, StateTransition);
        _StateBattle.ChangeState(null);
    }
    void StateTransition() {
        _StateBattle.ChangeState(null);
        SceneManager.LoadScene("Title", LoadSceneMode.Single);
    }
    #endregion
}
