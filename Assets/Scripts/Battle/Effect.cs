﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class Effect : MonoBehaviour {
    /// <summary>
    /// 爆発エフェクトを作成します
    /// </summary>
    /// <param name="transform">Transform.</param>
    public Effect(Transform transform) {
        string prefabPath = "Prefabs/FireExplosionEffects/Prefabs/SmallExplosionEffect";
        GameObject prefab = Resources.Load(prefabPath) as GameObject;
        if (prefab == null) {
            LogView.Log("存在しないPrefabファイル : " + prefabPath);
            return;
        }
        prefab = Instantiate(prefab, transform.position, transform.rotation);
        ParticleSystem ps = prefab.GetComponentInChildren<ParticleSystem>();
        if (ps == null) {
            return;
        }
        Destroy(prefab, (float)ps.main.duration);
    }
}
