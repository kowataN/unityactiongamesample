﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountDown : MonoBehaviour {
    private Animator _Animator;
    public bool _EndFlag;

    public delegate void executeState();
    public executeState ExecDelegate;

    // Use this for initialization
    void Start () {
        _Animator = this.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (_EndFlag && ExecDelegate != null) {
            ExecDelegate();
            ExecDelegate = null;
        }
	}
}
