﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;

public class MainStage : MonoBehaviour {
    [SerializeField]
    private float _Speed = 0.0f;
    [SerializeField]
    private GameObject _StageNode = null;
    [SerializeField]
    private GameObject _EnemyNode = null;
    [SerializeField]
    private GameObject _PlayerNode = null;
    public GameObject PlayerNode {
        get { return _PlayerNode; }
    }
    private GameObject _Player = null;
    // 敵リスト
    private List<Enemy> _EnemyList = new List<Enemy>();

    private int PlayerHp = 2;

    private void Awake() {
        LogView.Log("MainStage::Awake");
        _StageNode = GameObject.Find("StageNode");
        _EnemyNode = GameObject.Find("EnemyNode");
        _PlayerNode = GameObject.Find("PlayerNode");
    }

    private void Start() {
        LogView.Log("MainStage::Start");
        if (_StageNode == null) { LogView.Log("_StageNode is null"); }
        if (_EnemyNode == null) { LogView.Log("_EnemyNode is null"); }
        if (_PlayerNode == null) { LogView.Log("_PlayerNode is null"); }
    }

    public void Init() {
        LogView.Log("MainStage::Init");
        CreatePlayer();
        LoadStage();

        // 回転させる
        this.UpdateAsObservable()
            .Subscribe(_ => { _StageNode.transform.Rotate(new Vector3(0, _Speed, 0)); });
    }

    #region CreatePlayer
    private void CreatePlayer() {
        LogView.Log("MainStage::CreatePlayer");
        if (_PlayerNode == null) {
            LogView.Log("MainStage::CreatePlayer _PlayerNode is null");
            return;
        }

        Vector3 position = new Vector3(0, 2, 0);
        string prefabPath = "Prefabs/asobi_chan_b";
        _Player = Resources.Load(prefabPath) as GameObject;
        if (_Player == null) {
            LogView.Log("MainStage::CreatePlayer 存在しないPrefabファイル : " + prefabPath);
            return;
        }
        _Player = Instantiate(_Player, _PlayerNode.transform);
        _Player.transform.position = position;
        _Player.name = "Player"; // 「(clone)」が付与されるのでリネーム
        _Player.tag = "Player";

        GetPlayer().Init(PlayerHp);
    }
    #endregion

    #region LoadStage
    private void LoadStage() {
        LogView.Log("MainStage::LoadStage");

        if (_EnemyNode == null) {
            LogView.Log("EnemyNode is null");
            return;
        }

        // ステージ情報読み込み※Resouces配下にファイルを置く
        CsvLoader csvLoader = new CsvLoader();
        LogView.Log("GameLevel : " + GameMainManager.Instance.gameLevel);
        string path = "Datas/Stage" + string.Format("{0:D3}",
                                                    GameMainManager.Instance.gameLevel);
        bool res = csvLoader.Load(path, CsvLoader.TDelimiter.Tab);
        if (res == false) {
            LogView.Log("ファイルの読み込みに失敗しました。 PATH : " + path);
            return;
        }
        int headerCount = csvLoader.Headers.Count;
        Dictionary<int, List<string>> csvdata = csvLoader.GetDatas();
        if (csvdata.Count == 0) {
            LogView.Log("データが存在しません。");
            return;
        }

        LogView.Log("csv data count : " + csvdata.Count);

        int IndexX = csvLoader.GetHeaderIndexFromString("X"),
            IndexY = csvLoader.GetHeaderIndexFromString("Y"),
            IndexZ = csvLoader.GetHeaderIndexFromString("Z"),
            IndexPrefab = csvLoader.GetHeaderIndexFromString("PREFAB"),
            IndexIntervalType = csvLoader.GetHeaderIndexFromString("INTERVAL_TYPE"),
            IndexInterval = csvLoader.GetHeaderIndexFromString("SHOT_INTERVAL"),
            IndexShotSpeed = csvLoader.GetHeaderIndexFromString("SHOT_SPEED"),
            IndexTag = csvLoader.GetHeaderIndexFromString("TAG");
        foreach (KeyValuePair<int, List<string>> pair in csvdata) {
            if (headerCount != pair.Value.Count) {
                LogView.Log("データ不整合");
                continue;
            }
            Vector3 position = new Vector3(
                float.Parse(pair.Value[IndexX]),
                float.Parse(pair.Value[IndexY]),
                float.Parse(pair.Value[IndexZ])
            );
            string prefabPath = "Prefabs/";
            string fileName = pair.Value[IndexPrefab];
            GameObject prefab = Resources.Load(prefabPath + fileName) as GameObject;
            if (prefab == null) {
                LogView.Log("存在しないPrefabファイル : " + prefabPath + fileName);
                continue;
            }
            prefab = Instantiate(prefab, _EnemyNode.transform);
            prefab.tag = pair.Value[IndexTag];
            prefab.transform.position = position;
            Enemy.IntervalType type =
                pair.Value[IndexIntervalType].ToString() == "RANDOM"
                     ? Enemy.IntervalType.Random : Enemy.IntervalType.Normal; 
            Enemy enemy = prefab.GetComponent<Enemy>();
            enemy.Init(_Player, type, float.Parse(pair.Value[IndexInterval]), float.Parse(pair.Value[IndexShotSpeed]));
            _EnemyList.Add(enemy);
        }
    }
    #endregion

    public void StopEnemy() {
        ChangeEnemyState(Enemy.State.Stop);
    }
    public void StartEnemy() {
        ChangeEnemyState(Enemy.State.Run);
    }
    private void ChangeEnemyState(Enemy.State state) {
        foreach (Enemy enemy in _EnemyList) {
            enemy.state = state;
        }
    }

    public Player GetPlayer() {
        if (_Player == null) {
            return null;
        }

        return _Player.GetComponentInChildren<Player>();
    }
}
