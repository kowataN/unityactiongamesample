﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(Animator))]
public class Player : MonoBehaviour {
    private CharacterController _Chara = null;
    private Rigidbody _Rb = null;
    private Animator _Animator = null;
    //private float _InputHorizontal, _InputVertical;
    public float _MoveSpeed = 0.01f;

    // HP
    private IntReactiveProperty _HP = null;
    public IntReactiveProperty HP { get { return _HP; } }

    private void Awake() {
        LogView.Log("Player::Awake");

        InitAnimator();
        InitRigidBody();
        InitCharacterController();
    }
    public void Init(int hp) {
        LogView.Log("Player::Init");

        // HP設定
        _HP = new IntReactiveProperty(hp);

        // 弾との当たり判定条件
        var TriggerEnterPlayer = this.OnTriggerEnterAsObservable()
                                     .Select(collision => collision.tag)
                                     .Where(tag => tag == "Shot");

        // 弾にあったたらHP減算
        TriggerEnterPlayer.Subscribe(_ => {
            // HP減算
            _HP.Value = Mathf.Clamp(--_HP.Value, 0, _HP.Value);
            //LogView.Log("HP : " + _Hp.Value.ToString());
        });

        // 移動関連
        this.UpdateAsObservable()
            .Where(_ => InputManager.Instance.IsMove())
            .Subscribe(_ => UpdatePosition());

        this.UpdateAsObservable().Subscribe(_ => UpdateAnimation());
    }
    void InitAnimator() {
        _Animator = GetComponent<Animator>();
    }
    void InitRigidBody() {
        _Rb = GetComponent<Rigidbody>();
        if (_Rb == null) { return; }
        _Rb.isKinematic = true;
        _Rb.useGravity = true;
    }
    void InitCharacterController() {
        _Chara = GetComponent<CharacterController>();
        if (_Chara == null) { return; }
        _Chara.center = new Vector3(0, 0.88f, 0);
        _Chara.radius = 0.25f;
        _Chara.height = 1.6f;
    }
    /// <summary>
    /// 移動、回転処理
    /// </summary>
    void UpdatePosition() {
        // 移動
        Vector3 vector = new Vector3(InputManager.Instance.GetAxisKeyHorizontal() * _MoveSpeed,
                                     0, InputManager.Instance.GetAxisKeyVertical() * _MoveSpeed);
        // 回転
        UpdateRotation(vector);

        // 重力
        vector.y -= 9.8f * Time.deltaTime;
        _Chara.Move(vector);
    }
    /// <summary>
    /// 向き調整など
    /// </summary>
    void UpdateRotation(Vector3 vector) {
        // 回転
        Vector3 forward = Vector3.Slerp(transform.forward, vector,
            360 * Time.deltaTime / Vector3.Angle(transform.forward, vector));
        transform.LookAt(transform.position + forward);
    }
    /// <summary>
    /// アニメーション更新
    /// </summary>
    void UpdateAnimation() {
        _Animator.SetBool("Run", InputManager.Instance.IsMove());
    }
}
