﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class Shot : MonoBehaviour {
    /// <summary>
    /// 弾速
    /// </summary>
    private float _ShotSpeed = 10.0f;

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}
    public void Init(float Speed) {
        LogView.Log("Shot::Init SPD : " + Speed.ToString());
        _ShotSpeed = Speed;
        // 座標更新
        this.UpdateAsObservable()
            .Subscribe(_ => UpdateShot())
            .AddTo(gameObject);

        // 一定範囲外で破棄
        this.UpdateAsObservable()
            .Where(_ => (transform.position.x < -10 || transform.position.z < -10))
            .Subscribe(_ => Destroy(gameObject))
            .AddTo(gameObject);

        // プレイヤーに接触したら
        this.OnTriggerEnterAsObservable()
            .Select(collision => collision.tag)
            .Where(tag => tag == "Player")
            .Subscribe(_ => { AddEffect(); Destroy(gameObject); })
            .AddTo(gameObject);
    }
    /// <summary>
    /// 弾更新
    /// </summary>
    protected void UpdateShot() {
        Vector3 addPos = Vector3.forward * _ShotSpeed;
        float posY = transform.position.y;
        transform.position += transform.rotation * addPos * Time.deltaTime;
        Vector3 Target = new Vector3(transform.position.x, posY, transform.position.z);
        transform.position = Target;
    }
    /// <summary>
    /// 爆発エフェクトを作成する
    /// </summary>
    void AddEffect() {
        Effect eff = new Effect(this.transform);
    }
}
