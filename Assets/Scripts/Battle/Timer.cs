﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;
using UniRx.Triggers;

public class Timer : MonoBehaviour {
    private int _Interval;
    /// <summary>
    /// 秒を設定します
    /// </summary>
    private IntReactiveProperty _Second;
    public IReactiveProperty<int> Second { 
        get { return _Second; }
    }
    private enum State { Idle, Loop };
    private State _State = State.Idle;
    /// <summary>
    /// 初期化を行います
    /// </summary>
    /// <param name="time">Time.</param>
    /// <param name="_Interval">Interval.</param>
    public void InitTimer(int time, int _Interval) {
        _Second = new IntReactiveProperty(time);
        _State = State.Loop;

        // 毎秒減算
        Observable.Timer(TimeSpan.FromSeconds(_Interval),
                         TimeSpan.FromSeconds(_Interval))
                  .Where(_ => _State == State.Loop)
                  .Subscribe(_ => { _Second.Value = Mathf.Clamp(--_Second.Value, 0, _Second.Value); })
                  .AddTo(gameObject);
    }
    public void Stop() {
        _State = State.Idle;
    }
    public void ReStart() {
        _State = State.Loop;
    }
}
