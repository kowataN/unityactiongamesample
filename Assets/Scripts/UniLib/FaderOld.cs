﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class FaderOld : MonoBehaviour {
    /// <summary>
    /// フェードタイプ
    /// </summary>
    public enum FadeType {
        FadeIn, FadeOut
    }
    private static FadeType _FadeType;

    /// <summary>
    /// フェードの状態
    /// </summary>
    public enum FadeState {
        Idle, Fading, End
    }
    private static ReactiveProperty<FadeState> _State;
    public static IReactiveProperty<FadeState> state {
        get { return _State; }
    }
    /// <summary>
    /// フェード時間
    /// </summary>
    private static float _Duration;
    /// <summary>
    /// フェード開始色
    /// </summary>
    private static Color _StartColor;
    /// <summary>
    /// フェード目標色
    /// </summary>
    private static Color _TargetColor;
    /// <summary>
    /// 加算値
    /// </summary>
    private static Color _AddColor = Color.clear;

    private static Color _CrtColor = Color.clear;

    /// <summary>
    /// フェード用描画域
    /// </summary>
    private static Canvas _Canvas;
    private static Image _Image;

    private void Awake() {
        Debug.Log("Fader::Awake");
    }
    private static bool _IsInit = false;
    private static void Init() {
        if (_IsInit == true) {
            _State.Value = FadeState.Idle;
            return;
        }
        Debug.Log("Fader Init");
        GameObject fadeCanvasObj = new GameObject("FadeCanvas");
        _Canvas = fadeCanvasObj.AddComponent<Canvas>();
        fadeCanvasObj.AddComponent<GraphicRaycaster>();
        _Canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        fadeCanvasObj.AddComponent<Fader>();

        // 最前面に来るように
        _Canvas.sortingOrder = 1000;

        // フェード用のイメージ作成
        _Image = new GameObject("FadeImage").AddComponent<Image>();
        _Image.transform.SetParent(_Canvas.transform, false);
        _Image.rectTransform.anchoredPosition = Vector3.zero;
        _Image.rectTransform.sizeDelta =
                  new Vector2(Screen.currentResolution.width,
                              Screen.currentResolution.height);
        _Image.color = Color.clear;

        _State = new ReactiveProperty<FadeState>(FadeState.Idle);
        _IsInit = true;
    }

    private void Update() {
        Debug.Log("Update");
        _CrtColor += _AddColor * Time.deltaTime;
        _Image.color = _CrtColor;
        if (_FadeType == FadeType.FadeIn) {
            CheckFadeIn();
        } else if (_FadeType == FadeType.FadeOut) {
            CheckFadeOut();
        }
    }
    private static void CheckFadeIn() {
        // フェードイン
        if (_Image.color.a <= _TargetColor.a) {
            SetActiveFade(false);
            _State.Value = FadeState.End;
            _Image.color = _TargetColor;
            Debug.Log("CheckFadeIn  end " + _Image.color.ToString());
        }
    }
    private static void CheckFadeOut() {
        if (_Image.color.a >= _TargetColor.a) {
            SetActiveFade(true);
            _State.Value = FadeState.End;
            _Image.color = _TargetColor;
            Debug.Log("CheckFadeOut end " + _Image.color.ToString());
        }
    }

    public static void SetActiveFade(bool value) {
        _Canvas.gameObject.SetActive(value);
    }
    public static void StartFade(float duration, Color startColor, Color targetColor, FadeType type) {
        Init();
        if (_State.Value == FadeState.Fading) {
             return;
        }
        Debug.Log("Fader::StartFade");
        _Duration = duration;
        _StartColor = startColor;
        _TargetColor = targetColor;

        // 各色成分の加算値計算
        _AddColor = (_TargetColor - _StartColor) / duration;

        SetActiveFade(true);
        _State.Value = FadeState.Fading;
        _FadeType = type;
        _CrtColor = _Image.color = startColor;
    }
    public static void BlackOut(float duration) {
        StartFade(duration, Color.clear, Color.black, FadeType.FadeOut);
    }
    public static void BlackIn(float duration) {
        StartFade(duration, Color.black, Color.clear, FadeType.FadeIn);
    }
    public static void WhiteOut(float duration) {
        StartFade(duration, Color.clear, Color.white, FadeType.FadeOut);
    }
    public static void WhiteIn(float duration) {
        StartFade(duration, Color.white, Color.clear, FadeType.FadeIn);
    }
}
