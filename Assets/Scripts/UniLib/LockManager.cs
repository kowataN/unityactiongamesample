﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LockManager : SingletonBase<LockManager> {
    private int _LockCount = 0;

    public void Lock() {
        ++_LockCount;
    }
    public void UnLock() {
        --_LockCount;
    }
    public bool IsLock() {
        return _LockCount != 0;
    }
    public bool IsUnLock() {
        return _LockCount == 0;
    }
}
