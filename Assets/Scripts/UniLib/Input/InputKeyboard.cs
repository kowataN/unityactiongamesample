﻿using UnityEngine;

/// <summary>
/// キーボード入力クラス
/// </summary>
public class InputKeyborad : IInputBase {
    private float _InputHorizontal;
    /// <summary>
    /// 水平方向への移動量
    /// </summary>
    public float InputHorizontal {
        get { return _InputHorizontal; }
    }
    private float _InputVertical;
    /// <summary>
    /// 垂直方向への移動量
    /// </summary>
    public float InputVertical {
        get { return _InputVertical; }
    }
    /// <summary>
    /// 入力情報を更新します
    /// </summary>
    public void UpdateInput() {
        //LogView.Log("InputKeyborad::UpdateInput");
        _InputHorizontal = Input.GetAxis("Horizontal");
        _InputVertical = Input.GetAxis("Vertical");
    }
    /// <summary>
    /// 入力情報を初期化します
    /// </summary>
    public void InitInput() {
        //LogView.Log("InputKeyborad::InitInput");
        _InputHorizontal = _InputVertical = 0.0f;
    }
}
