﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using UniRx.Triggers;

public class InputManager : SingletonMonoBhv<InputManager> {
    // TODO:
    // Lockクラスを作成する※参照カウンタみたいなもの

    #region キー入力関連
    private BoolReactiveProperty _EnabledKey = new BoolReactiveProperty(false);
    /// <summary>
    /// キーボード有効かどうか
    /// </summary>
    public IReadOnlyReactiveProperty<bool> EnabledKey {
        get { return _EnabledKey; }
    }
    private InputKeyborad _InputKey = new InputKeyborad();
    #endregion

    #region マウス関連
    private BoolReactiveProperty _EnabledMouse = new BoolReactiveProperty(true);
    /// <summary>
    /// マウスを使用するか
    /// </summary>
    public IReadOnlyReactiveProperty<bool> EnabledMouse {
        get { return _EnabledMouse; }
    }
    private InputMouse _InputMouse = new InputMouse();
    #endregion

    /// <summary>
    /// マウスクリックと画面タッチの座標を一緒に扱う
    /// </summary>
    private Vector3ReactiveProperty _TouchPosition = new Vector3ReactiveProperty(Vector3.zero);
    public IReadOnlyReactiveProperty<Vector3> TouchPosition {
        get { return _TouchPosition; }
    }

    /// <summary>
    /// 入力関連を設定します
    /// </summary>
    public void Init() {
        // キーボードが有効状態のみ更新を行う
        this.UpdateAsObservable()
            .Where(_ => _EnabledKey.Value)
            .Subscribe(_ => { _InputKey.UpdateInput(); })
            .AddTo(gameObject);

        // キーボードが無効になったら初期化する
        _EnabledKey.Where(_ => EnabledKey.Value == false)
                   .Subscribe(_ => { _InputKey.InitInput(); });

        // マウスが有効状態のみ更新を行う
        this.UpdateAsObservable()
            .Where(_ => _EnabledMouse.Value)
            .Subscribe(_ => {
                _InputMouse.UpdateInput(); // 入力情報更新
                _TouchPosition.Value = _InputMouse.TouchPosition; // タッチ座標更新
            })
            .AddTo(gameObject);

        // マウスが無効になったら初期化をする
        _EnabledMouse.Where(_ => _EnabledMouse.Value == false)
                     .Subscribe(_=> { _InputMouse.InitInput(); });
    }

    /// <summary>
    /// キーボードの有効状態を設定します
    /// </summary>
    /// <param name="flag">If set to <c>true</c> flag.</param>
    public void SetEnabledKeyborad(bool flag) {
        _EnabledKey.Value = flag;
    }
    /// <summary>
    /// マウスを有効状態を設定します
    /// </summary>
    /// <param name="flag">If set to <c>true</c> flag.</param>
    public void SetEnabledMouse(bool flag) {
        _EnabledMouse.Value = flag;
    }
    /// <summary>
    /// 水平方向の移動量を返します
    /// </summary>
    /// <returns>The axis key horizontal.</returns>
    public float GetAxisKeyHorizontal() {
        return _InputKey.InputHorizontal;
    }
    /// <summary>
    /// 垂直方向の移動量を返します
    /// </summary>
    /// <returns>The axis key vertical.</returns>
    public float GetAxisKeyVertical() {
        return _InputKey.InputVertical;
    }
    public bool IsMove() {
        return (_InputKey.InputHorizontal != 0 || _InputKey.InputVertical != 0);
                //|| _InputMouse.TouchPosition != Vector3.zero);
    }
}
