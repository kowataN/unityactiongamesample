using UnityEngine;
using System.Collections;

public class SingletonBase<T> where T : class, new() {
    private static readonly T _Instance = new T();
    public static T Instance { get { return _Instance; } }
}
