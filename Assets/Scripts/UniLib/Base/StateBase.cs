﻿using System.Collections;
using UnityEngine;

namespace State {
    /// <summary>
    /// ステート基底クラス
    /// </summary>
    public abstract class StateBase {
        public delegate void executeState();
        public executeState ExecDelegate;

        public bool endFlag { set; get; }

        public StateBase(executeState exectute) {
            ChangeState(exectute);
        }

        public abstract void Execute();

        public void ChangeState(executeState execute) {
            ExecDelegate = execute;
            endFlag = false;
        }
    }

    /// <summary>
    /// サブステート
    /// </summary>
    public class SubState : StateBase {
        public SubState(executeState execute) : base(execute) { }
        public override void Execute() {
            if (ExecDelegate != null) {
                ExecDelegate();
            }
        }
    }
}
