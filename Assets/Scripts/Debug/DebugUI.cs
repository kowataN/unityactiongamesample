﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UniRx;

public class DebugUI : SingletonMonoBhv<DebugUI> {
    public GameObject _PlayerNode;
    public GameObject _MainUI;
    public GameObject _Player;
    public GameObject _Timer;

    private Button _BtnInputKey = null;
    private Vector3 _InitPos = Vector3.zero;
    private BattleMain _Main = null;

    public void Init() {
        InitBtnInitPos();
        InitBtnInputKey();
        InitBtnTimer();
        InitBtnStop();
    }

    #region プレイヤー関連
    /// <summary>
    /// プレイヤー位置初期化ボタンを初期化します
    /// </summary>
    void InitBtnInitPos() {
        if (_PlayerNode == null) {
            Debug.Log("_PlayerNode is null");
            return;
        }
        _Player = _PlayerNode.transform.Find("Player").gameObject;
        if (_Player == null) {
            LogView.Log("DebugUI::_Player is null");
            return;
        }
        _InitPos = _Player.transform.position;

        Transform trans = transform.Find("BtnInitPos");
        if (trans == null) {
            LogView.Log("DebugUI::trans is null");
            return;
        }
        LogView.Log("DebugUI::InitBtnInitPos end");

        Button btn = trans.GetComponent<Button>();
        if (btn == null) { return; }

        btn.OnClickAsObservable()
           .Subscribe(_ => { _Player.transform.position = _InitPos; })
           .AddTo(gameObject);
    }
    #endregion

    #region キー入力関連
    /// <summary>
    /// キー入力ボタンを初期化します
    /// </summary>
    void InitBtnInputKey() {
        Transform trans = transform.Find("BtnInputKey");
        if (trans == null) { return; }

        _BtnInputKey = trans.GetComponent<Button>();
        if (_BtnInputKey == null) { return; }

        //UpdateInputKeyLabel();
        _BtnInputKey.OnClickAsObservable()
                    .Subscribe(_ => UpdateInputKeyLabel())
                    .AddTo(gameObject);
    }

    void UpdateInputKeyLabel() {
        // 単純にフラグを反転
        InputManager.Instance.SetEnabledKeyborad(!InputManager.Instance.EnabledKey.Value);
        Text label = _BtnInputKey.GetComponentInChildren<Text>();
        if (label != null) {
            label.text = "キー入力：" + InputManager.Instance.EnabledKey.ToString();
        } else {
            LogView.Log("_BtnInputKey: is not text");
        }
    }
    #endregion

    #region タイマー関連
    void InitBtnTimer() {
        Transform trnas = transform.Find("BtnInitTimer");
        if (trnas == null) { return; }

        Button btn = trnas.GetComponent<Button>();
        if (btn == null) { return; }

        //btn.OnClickAsObservable()
           //.Subscribe()
           //.AddTo(gameObject);
    }
    #endregion

    #region 停止
    enum StopState { Start, Stop }
    private StopState _StopState = StopState.Start;
    void InitBtnStop() {
        Transform trans = transform.Find("BtnStop");
        if (trans == null) { return; }

        Button btn = trans.GetComponent<Button>();
        if (btn == null) { return; }

        _Main = _MainUI.GetComponentInChildren<BattleMain>();
        btn.OnClickAsObservable()
           .Subscribe(_ => ChageTimerAndEnemy())
           .AddTo(gameObject);

        Text label = btn.GetComponentInChildren<Text>();
        if (label != null) {
            label.text = "敵＆タイマー：" + _StopState.ToString();
        }
    }
    void ChageTimerAndEnemy() {
        _StopState = _StopState == StopState.Start ? StopState.Stop : StopState.Start;
        if (_StopState == StopState.Start) {
            _Main.ReStart();
        } else {
            _Main.Stop();
        }
    }
    #endregion
}
