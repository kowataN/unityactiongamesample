﻿using UnityEngine;
using UnityEngine.UI;
using System;
using UniRx;
using DG.Tweening;

public class TitleDebug : MonoBehaviour {
    [SerializeField] Button _BtnFadeBlack = null;
    [SerializeField] Button _BtnFadeWhite = null;
    [SerializeField] Button _BtnFadeSepia = null;

    private void Awake() {}

    // Use this for initialization
    void Start() {
        Debug.Log("TitleDebug::Start");
        _BtnFadeBlack.OnClickAsObservable()
                     .Subscribe(_ => OnClickFadeBlack())
                     .AddTo(gameObject);

        _BtnFadeWhite.OnClickAsObservable()
                     .Subscribe(_ => OnClickFadeWhite())
                     .AddTo(gameObject);

        _BtnFadeSepia.OnClickAsObservable()
                     .Subscribe(_ => OnClickFadeSepia())
                     .AddTo(gameObject);
    }

    // Update is called once per frame
    void Update() { }

    void DisabledFaderActive() {
        Debug.Log("TitleDebug::DisabledFaderActive time:" + System.DateTime.Now.ToString());
        Fader.Instance.SetActiveFade(false);
    }

    void OnClickFadeBlack() {
        Debug.Log("TitleDebug::OnClickFadeBlack");
        Fader.Instance.BlackOut(2.0f, BlackFadeIn);
    }
    void BlackFadeIn() {
        Debug.Log("TitleDebug::BlackFadeIn");
        Fader.Instance.BlackIn(2.0f, DisabledFaderActive);
    }

    void OnClickFadeWhite() {
        Debug.Log("TitleDebug::OnClickFadeWhite time:" + System.DateTime.Now.ToString());
        Fader.Instance.WhiteOut(1.0f, WhiteFadeIn);
    }
    void WhiteFadeIn() {
        Debug.Log("TitleDebug::WhiteFadeIn time : " + System.DateTime.Now.ToString());
        Fader.Instance.WhiteIn(2.0f, DisabledFaderActive);
    }

    private Color _SepiaColor = new Color(117.0f / 255.0f, 83.0f / 255.0f, 37.0f / 255.0f, 0.5f);
    void OnClickFadeSepia() {
        Fader.Instance.StartFade(2.0f, Color.clear, _SepiaColor, SepiaFadeIn);
    }
    void SepiaFadeIn() {
        Fader.Instance.StartFade(2.0f, _SepiaColor, Color.clear, DisabledFaderActive);
    }
}
