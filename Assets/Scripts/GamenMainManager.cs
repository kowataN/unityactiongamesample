﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMainManager : SingletonBase<GameMainManager> {
    /// <summary>
    /// ゲーム難易度
    /// </summary>
    private int _GameLevel = 3;
    /// <summary>
    /// 難易度
    /// </summary>
    public int gameLevel {
        get { return _GameLevel; }
        set { _GameLevel = value; }
    }
}
