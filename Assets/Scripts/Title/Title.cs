﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UniRx;
using State;

public class Title : MonoBehaviour {
    // 適当に変数追加
    public Button _BtnGame1;
    public Button _BtnGame2;
    public Button _BtnGame3;
    public Button _BtnGame4;

    private StateTitle _StateTitle;

    private void Awake() {}

    // Use this for initialization
    private void Start() {
        Fader.Instance.Init();
        Fader.Instance.SetColorAndActive(Color.white, true);
        _StateTitle = new StateTitle(StateWhiteIn);
    }

    // Update is called once per frame
    private void Update () {
        _StateTitle.Execute();
    }

    #region state funciton
    /// <summary>
    /// ホワイトイン開始状態
    /// </summary>
    private void StateWhiteIn() {
        Debug.Log("***** StateWhiteIn *****");
        // フェードインが終わったら初期化状態にする
        Fader.Instance.WhiteIn(3.0f, StateInit);
        _StateTitle.ChangeState(null);
    }
    /// <summary>
    /// 初期化状態
    /// </summary>
    private void StateInit() {
        Debug.Log("***** StateInit *****");

        Fader.Instance.SetActiveFade(false);

        // ボタン押下で難易度設定
        _BtnGame1.OnClickAsObservable()
                 .Subscribe(_ => {
                     TransitionScene(1);
                     _StateTitle.ChangeState(StateWhiteOut);
                 })
                 .AddTo(gameObject);
        _BtnGame2.OnClickAsObservable()
                 .Subscribe(_ => {
                     TransitionScene(2);
                     _StateTitle.ChangeState(StateWhiteOut);
                 })
                 .AddTo(gameObject);
        _BtnGame3.OnClickAsObservable()
                 .Subscribe(_ => {
                     TransitionScene(3);
                     _StateTitle.ChangeState(StateWhiteOut);
                 })
                 .AddTo(gameObject);
        _BtnGame4.OnClickAsObservable()
                 .Subscribe(_ => {
                     TransitionScene(4);
                     _StateTitle.ChangeState(StateWhiteOut);
                 })
                 .AddTo(gameObject);
        // 待機状態にする
        _StateTitle.ChangeState(StateIdle);
    }
    /// <summary>
    /// 待機状態
    /// </summary>
    private void StateIdle() {
        // 何もしない
    }
    /// <summary>
    /// ホワイトアウト状態
    /// </summary>
    private void StateWhiteOut() {
        Debug.Log("***** StateWhiteOut *****");
        Fader.Instance.BlackOut(2.0f, StateTransition);
        _StateTitle.ChangeState(null);
    }
    private void StateTransition() {
        Debug.Log("***** StateTransition *****");
        SceneManager.LoadScene("Battle", LoadSceneMode.Single);
        // ステートマシン停止
        _StateTitle.ChangeState(null);
    }
    #endregion

    private void TransitionScene(int level) {
        Debug.Log("***** TransitionScene *****");
        GameMainManager.Instance.gameLevel = level;
    }
}
