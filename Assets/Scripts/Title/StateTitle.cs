﻿/* 
 * タイトル状態遷移
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using State;

public class StateTitle : StateBase {
    public StateTitle(executeState execute) : base(execute) {
        Debug.Log("***** StateTitle::StateTitle");
    }
    public override void Execute() {
        if (ExecDelegate != null) {
            ExecDelegate();
        }
    }
}
